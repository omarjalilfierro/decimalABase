package app;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sn = new Scanner(System.in);
		System.out.println("Escriba su numero en decimal y luego el numero de la base que desee \n"
				+ "ej. 2 para decimal \n"
				+ "    16 para hexadecimal");
		int num = sn.nextInt();
		int base = sn.nextInt();
		
		if (base == 2) System.out.println("El numero : " + num + " en binario es: " +  Base.base(num, base));
		else if (base == 8) System.out.println("El numero : " + num + " en octal es: " +  Base.base(num, base));
		else if (base == 10) System.out.println("El numero : " + num + " en decimal es: " +  Base.base(num, base));
		else if (base == 16) System.out.println("El numero : " + num + " en hexadecimal es: " +  Base.base(num, base));
		else
			System.out.println("El numero : " + num + " en base "+ base +" es: " +  Base.base(num, base));

		
		
	}

}
